//
//  Wander+CoreDataClass.swift
//  Wandr
//
//  Created by Jun Zhou on 3/13/18.
//  Copyright © 2018 Wandr. All rights reserved.
//
//

import Foundation
import CoreStore
import SwiftyJSON

@objc(Wander)
public class Wander: NSManagedObject, JSONAbleType {
    func fromJSONWithTransaction(_ json: JSON, keyValue: String, transaction: AsynchronousDataTransaction) {
        self.wanderId = keyValue
        let placeTypes = json["placeTypes"].arrayValue
        self.placeTypes = getPlaceTypes(transaction: transaction, jsons: placeTypes)
        let activities = json["activities"].arrayValue
        let tempActivities = getActivities(transaction: transaction, jsons: activities, categoryJsons: placeTypes)
        self.wandrActivities = tempActivities
        let author = json["author"].stringValue
        self.author = author
        let locations = json["location"].arrayValue
        let tempLocation = getLocation(transaction: transaction, json: locations)
        self.location = tempLocation
        let map = json["map"]
        self.map = getMap(transaction: transaction, json: map)
        let radius = json["radius"].int16Value
        self.radius = radius
    }
    
    func getActivities(transaction: AsynchronousDataTransaction, jsons: [JSON], categoryJsons: [JSON]) -> NSSet {
        let temp = NSMutableSet();
        for (index, json) in jsons.enumerated() {
            let wandrActivity = getActivity(transaction: transaction, json: json, categoryJson: categoryJsons[index])
            temp.add(wandrActivity)
        }
        return temp
    }
    
    func getActivity(transaction: AsynchronousDataTransaction, json: JSON, categoryJson: JSON) -> WandrActivity {
        let wandrActivity = transaction.create(Into<WandrActivity>())
        let geomotry = json["geometry"]
        wandrActivity.geomotry = getGeometry(transaction: transaction, json: geomotry)
        let iconUrl = json["icon"].stringValue
        let icon = transaction.create(Into<Icon>());
        icon.iconUrl = iconUrl
        if let url = URL(string: iconUrl) {
            icon.icon = try? Data(contentsOf: url) as NSData
        }
        let id = json["id"].stringValue
        wandrActivity.wandrActivityId = id
        let name = json["name"].stringValue
        wandrActivity.name = name
        let opening_hours = json["opening_hours"]
        wandrActivity.openingHours = getOpenHours(transaction: transaction, json: opening_hours)
        let photos = json["photos"].arrayValue
        let photoSet = getPhotoData(transaction: transaction, jsons: photos)
        wandrActivity.photos = photoSet;
        let placeId = json["place_id"].stringValue
        wandrActivity.placeId = placeId
        let rating = json["rating"].doubleValue
        wandrActivity.rating = rating
        let reference = json["reference"].stringValue
        wandrActivity.reference = reference
        let scope = json["scope"].stringValue
        wandrActivity.scope = scope
        let types = json["types"].arrayValue
        let typeSet = NSMutableSet()
        for type in types {
            typeSet.add(type.stringValue)
        }
        wandrActivity.types = typeSet
        let vicinity = json["vicinity"].stringValue
        wandrActivity.vicinity = vicinity
        let type = categoryJson.stringValue
        wandrActivity.wandrCategory = getWandrCategory(transaction: transaction, type: type)
        wandrActivity.like = true
        return wandrActivity
    }
    
    func getGeometry(transaction: AsynchronousDataTransaction, json: JSON) -> Geometry {
        let location = json["location"]
        let geometry = transaction.create(Into<Geometry>())
        geometry.latitude = location["lat"].stringValue
        geometry.longnitude = location["lng"].stringValue
        return geometry
    }
    
    func getPhotoData(transaction: AsynchronousDataTransaction, jsons: [JSON]) -> NSSet {
        let photoSet = NSMutableSet()
        for json in jsons {
            let height = json["height"].int16Value
            let width = json["width"].int16Value
            let photoReference = json["photo_reference"].stringValue
            let photo = transaction.create(Into<Photo>())
            photo.height = height
            photo.width = width
            photo.reference = photoReference
            photoSet.add(photo)
        }
        return photoSet
    }
    
    func getOpenHours(transaction: AsynchronousDataTransaction, json: JSON) -> Hours {
        let hours = transaction.create(Into<Hours>())
        hours.openNow = json["open_now"].boolValue
        return hours
    }
    
    func getLocation(transaction: AsynchronousDataTransaction, json: [JSON]) -> Geometry {
        let location = transaction.create(Into<Geometry>())
        location.latitude = json[0].stringValue
        location.longnitude = json[1].stringValue
        return location
    }
    
    func getMap(transaction: AsynchronousDataTransaction, json: JSON) -> Map {
        let map = transaction.create(Into<Map>())
        let destinationAddresses = json["destination_addresses"].arrayValue
        let originAddresses = json["origin_addresses"].arrayValue
        let rows = json["rows"].arrayValue
        var count = 0
        let mapElements = NSMutableSet()
        while count < destinationAddresses.count {
            let destinationAddress = destinationAddresses[count].stringValue
            let originAddress = originAddresses[count].stringValue
            let row = rows[count]["elements"][count]
            let mapElement = getMapElement(transaction: transaction, json: row, destinationAddress: destinationAddress, originAddress: originAddress)
            mapElements.add(mapElement)
            count = count + 1
        }
        map.mapElements = mapElements
        let status = json["status"].stringValue
        map.status = status
        return map
    }
  
    func getMapElement(transaction: AsynchronousDataTransaction, json: JSON, destinationAddress: String, originAddress: String) -> MapElement {
        let mapElement = transaction.create(Into<MapElement>())
        mapElement.destinationAddress = destinationAddress
        mapElement.originAddress = originAddress
        let distance = getMapElementValue(json: json["distance"])
        mapElement.distanceText = distance.0
        mapElement.distance = distance.1
        let duration = getMapElementValue(json: json["duration"])
        mapElement.durationText = duration.0
        mapElement.duration = duration.1
        return mapElement
    }
    
    func getMapElementValue(json: JSON) -> (String, Double){
        let distanceText = json["text"].stringValue
        let distanceValue = json["value"].doubleValue
        return (distanceText, distanceValue)
    }
    
    func getPlaceTypes(transaction: AsynchronousDataTransaction, jsons: [JSON]) -> NSSet {
        let typeSet = NSMutableSet()
        let from = From<WandrCategory>()
        if let wandrCategories = transaction.fetchAll(from) {
            for json in jsons {
                let type = json.stringValue
                let wandrCategory = wandrCategories.filter{ $0.categoryId == type }[0]
                typeSet.add(wandrCategory)
            }

        }
        return typeSet
    }
    
    func getWandrCategory(transaction: AsynchronousDataTransaction, type: String) -> WandrCategory {
        let from = From<WandrCategory>()
        if let wandrCategories = transaction.fetchAll(from) {
            return wandrCategories.filter{ $0.categoryId == type }[0]
        }
        let temp = transaction.create(Into<WandrCategory>())
        temp.categoryId = "Unknown"
        return temp
    }
}
