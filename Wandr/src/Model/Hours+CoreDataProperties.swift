//
//  Hours+CoreDataProperties.swift
//  Wandr
//
//  Created by Jun Zhou on 3/13/18.
//  Copyright © 2018 Wandr. All rights reserved.
//
//

import Foundation
import CoreData


extension Hours {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Hours> {
        return NSFetchRequest<Hours>(entityName: "Hours")
    }

    @NSManaged public var close: String?
    @NSManaged public var open: String?
    @NSManaged public var period: String?
    @NSManaged public var openNow: Bool
    @NSManaged public var wandrActivity: WandrActivity?

}
