//
//  WandrActivity+CoreDataProperties.swift
//  Wandr
//
//  Created by Jun Zhou on 3/13/18.
//  Copyright © 2018 Wandr. All rights reserved.
//
//

import Foundation
import CoreData


extension WandrActivity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WandrActivity> {
        return NSFetchRequest<WandrActivity>(entityName: "WandrActivity")
    }

    @NSManaged public var author: String?
    @NSManaged public var formattedAddress: String?
    @NSManaged public var wandrActivityId: String?
    @NSManaged public var name: String?
    @NSManaged public var order: Int16
    @NSManaged public var placeId: String?
    @NSManaged public var rating: Double
    @NSManaged public var reference: String?
    @NSManaged public var scope: String?
    @NSManaged public var vicinity: String?
    @NSManaged public var types: NSObject?
    @NSManaged public var address: NSSet?
    @NSManaged public var geomotry: Geometry?
    @NSManaged public var openingHours: Hours?
    @NSManaged public var map: Map?
    @NSManaged public var photos: NSSet?
    @NSManaged public var wandrCategory: WandrCategory?
    @NSManaged public var viewPorts: NSSet?
    @NSManaged public var icon: Icon?
    @NSManaged public var wander: Wander?
    @NSManaged public var like: NSNumber?
}

// MARK: Generated accessors for address
extension WandrActivity {

    @objc(addAddressObject:)
    @NSManaged public func addToAddress(_ value: Location)

    @objc(removeAddressObject:)
    @NSManaged public func removeFromAddress(_ value: Location)

    @objc(addAddress:)
    @NSManaged public func addToAddress(_ values: NSSet)

    @objc(removeAddress:)
    @NSManaged public func removeFromAddress(_ values: NSSet)

}

// MARK: Generated accessors for map
extension WandrActivity {

    @objc(addMapObject:)
    @NSManaged public func addToMap(_ value: Map)

    @objc(removeMapObject:)
    @NSManaged public func removeFromMap(_ value: Map)

    @objc(addMap:)
    @NSManaged public func addToMap(_ values: NSSet)

    @objc(removeMap:)
    @NSManaged public func removeFromMap(_ values: NSSet)

}

// MARK: Generated accessors for photos
extension WandrActivity {

    @objc(addPhotosObject:)
    @NSManaged public func addToPhotos(_ value: Photo)

    @objc(removePhotosObject:)
    @NSManaged public func removeFromPhotos(_ value: Photo)

    @objc(addPhotos:)
    @NSManaged public func addToPhotos(_ values: NSSet)

    @objc(removePhotos:)
    @NSManaged public func removeFromPhotos(_ values: NSSet)

}

// MARK: Generated accessors for viewPorts
extension WandrActivity {

    @objc(addViewPortsObject:)
    @NSManaged public func addToViewPorts(_ value: ViewPort)

    @objc(removeViewPortsObject:)
    @NSManaged public func removeFromViewPorts(_ value: ViewPort)

    @objc(addViewPorts:)
    @NSManaged public func addToViewPorts(_ values: NSSet)

    @objc(removeViewPorts:)
    @NSManaged public func removeFromViewPorts(_ values: NSSet)

}
