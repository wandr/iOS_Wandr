//
//  Geometry+CoreDataProperties.swift
//  Wandr
//
//  Created by Jun Zhou on 3/13/18.
//  Copyright © 2018 Wandr. All rights reserved.
//
//

import Foundation
import CoreData


extension Geometry {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Geometry> {
        return NSFetchRequest<Geometry>(entityName: "Geometry")
    }

    @NSManaged public var latitude: String?
    @NSManaged public var longnitude: String?
    @NSManaged public var wandrActivity: WandrActivity?
    @NSManaged public var wander: Wander?

}
