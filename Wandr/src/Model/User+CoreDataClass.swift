//
//  User+CoreDataClass.swift
//  Wandr
//
//  Created by Jun Zhou on 3/13/18.
//  Copyright © 2018 Wandr. All rights reserved.
//
//

import Foundation
import CoreData
import SwiftyJSON

@objc(User)
public class User: NSManagedObject, JSONAbleType {
    func fromJSON(_ json: JSON, keyValue: String) {
        self.firstName = json["firstName"].stringValue
        self.lastName = json["lastName"].stringValue
        self.emailAddress = json["emailAddress"].stringValue
        let birthDate = json["birthDate"].doubleValue
        self.birthDate = Date(timeIntervalSince1970: birthDate) as NSDate
        self.pid = keyValue
        let profileImage = json["profileImage"].stringValue
        self.profileImage = NSData(base64Encoded: profileImage, options: .ignoreUnknownCharacters)
    }
}
