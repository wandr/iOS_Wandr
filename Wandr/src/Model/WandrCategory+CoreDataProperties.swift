//
//  WandrCategory+CoreDataProperties.swift
//  Wandr
//
//  Created by Jun Zhou on 3/13/18.
//  Copyright © 2018 Wandr. All rights reserved.
//
//

import Foundation
import CoreData


extension WandrCategory {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WandrCategory> {
        return NSFetchRequest<WandrCategory>(entityName: "WandrCategory")
    }

    @NSManaged public var category: String?
    @NSManaged public var categoryId: String?
    @NSManaged public var name: String?
    @NSManaged public var wandrActivity: WandrActivity?

}
