//
//  UsaCity+CoreDataProperties.swift
//  Wandr
//
//  Created by Jun Zhou on 4/4/18.
//  Copyright © 2018 Wandr. All rights reserved.
//
//

import Foundation
import CoreData


extension UsaCity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UsaCity> {
        return NSFetchRequest<UsaCity>(entityName: "UsaCity")
    }

    @NSManaged public var name: String?
    @NSManaged public var state: String?
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double

}
