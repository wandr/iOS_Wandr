//
//  MapElement+CoreDataProperties.swift
//  Wandr
//
//  Created by Jun Zhou on 3/13/18.
//  Copyright © 2018 Wandr. All rights reserved.
//
//

import Foundation
import CoreData


extension MapElement {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MapElement> {
        return NSFetchRequest<MapElement>(entityName: "MapElement")
    }

    @NSManaged public var destinationAddress: String?
    @NSManaged public var originAddress: String?
    @NSManaged public var distance: Double
    @NSManaged public var distanceText: String?
    @NSManaged public var duration: Double
    @NSManaged public var durationText: String?
    @NSManaged public var map: Map?

}
