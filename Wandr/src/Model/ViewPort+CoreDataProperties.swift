//
//  ViewPort+CoreDataProperties.swift
//  Wandr
//
//  Created by Jun Zhou on 3/13/18.
//  Copyright © 2018 Wandr. All rights reserved.
//
//

import Foundation
import CoreData


extension ViewPort {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ViewPort> {
        return NSFetchRequest<ViewPort>(entityName: "ViewPort")
    }

    @NSManaged public var viewPort: String?
    @NSManaged public var latitude: String?
    @NSManaged public var longnitude: String?
    @NSManaged public var wandrActivity: WandrActivity?

}
