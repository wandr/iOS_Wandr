//
//  Wander+CoreDataProperties.swift
//  Wandr
//
//  Created by Jun Zhou on 3/13/18.
//  Copyright © 2018 Wandr. All rights reserved.
//
//

import Foundation
import CoreData


extension Wander {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Wander> {
        return NSFetchRequest<Wander>(entityName: "Wander")
    }

    @NSManaged public var wanderId: String?
    @NSManaged public var radius: Int16
    @NSManaged public var author: String?
    @NSManaged public var wandrActivities: NSSet?
    @NSManaged public var location: Geometry?
    @NSManaged public var map: Map?
    @NSManaged public var placeTypes: NSSet?

}

// MARK: Generated accessors for wandrActivities
extension Wander {

    @objc(addWandrActivitiesObject:)
    @NSManaged public func addToWandrActivities(_ value: WandrActivity)

    @objc(removeWandrActivitiesObject:)
    @NSManaged public func removeFromWandrActivities(_ value: WandrActivity)

    @objc(addWandrActivities:)
    @NSManaged public func addToWandrActivities(_ values: NSSet)

    @objc(removeWandrActivities:)
    @NSManaged public func removeFromWandrActivities(_ values: NSSet)

}

// MARK: Generated accessors for placeTypes
extension Wander {

    @objc(addPlaceTypesObject:)
    @NSManaged public func addToPlaceTypes(_ value: WandrCategory)

    @objc(removePlaceTypesObject:)
    @NSManaged public func removeFromPlaceTypes(_ value: WandrCategory)

    @objc(addPlaceTypes:)
    @NSManaged public func addToPlaceTypes(_ values: NSSet)

    @objc(removePlaceTypes:)
    @NSManaged public func removeFromPlaceTypes(_ values: NSSet)

}
