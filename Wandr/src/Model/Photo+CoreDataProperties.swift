//
//  Photo+CoreDataProperties.swift
//  Wandr
//
//  Created by Jun Zhou on 3/13/18.
//  Copyright © 2018 Wandr. All rights reserved.
//
//

import Foundation
import CoreData


extension Photo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Photo> {
        return NSFetchRequest<Photo>(entityName: "Photo")
    }

    @NSManaged public var photo: NSData?
    @NSManaged public var photoId: String?
    @NSManaged public var photoName: String?
    @NSManaged public var photoUrl: String?
    @NSManaged public var reference: String?
    @NSManaged public var height: Int16
    @NSManaged public var width: Int16
    @NSManaged public var wandrActivity: WandrActivity?

}
