//
//  Location+CoreDataProperties.swift
//  Wandr
//
//  Created by Jun Zhou on 3/13/18.
//  Copyright © 2018 Wandr. All rights reserved.
//
//

import Foundation
import CoreData


extension Location {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Location> {
        return NSFetchRequest<Location>(entityName: "Location")
    }

    @NSManaged public var address: String?
    @NSManaged public var address1: String?
    @NSManaged public var address2: String?
    @NSManaged public var city: String?
    @NSManaged public var country: String?
    @NSManaged public var locationId: String?
    @NSManaged public var postCode: String?
    @NSManaged public var state: String?
    @NSManaged public var wandrActivity: WandrActivity?

}
