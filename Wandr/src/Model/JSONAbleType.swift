//
//  JSONAbleType.swift
//  Wandr
//
//  Created by Jun Zhou on 2/20/18.
//  Copyright © 2018 Wandr. All rights reserved.
//
import SwiftyJSON
import CoreStore

protocol JSONAbleType {
    func fromJSON(_: JSON, keyValue: String)
    func fromJSONWithTransaction(_ :JSON, keyValue: String, transaction: AsynchronousDataTransaction)
}

extension JSONAbleType {
    func fromJSON(_: JSON, keyValue: String) {
        
    }
    
    func fromJSONWithTransaction(_ :JSON, keyValue: String, transaction: AsynchronousDataTransaction) {
        
    }
}
