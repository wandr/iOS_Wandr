//
//  WandrCategory+CoreDataClass.swift
//  Wandr
//
//  Created by Jun Zhou on 3/13/18.
//  Copyright © 2018 Wandr. All rights reserved.
//
//

import Foundation
import CoreData
import SwiftyJSON

@objc(WandrCategory)
public class WandrCategory: NSManagedObject, JSONAbleType {
    func fromJSON(_ json: JSON, keyValue: String) {
        self.categoryId = keyValue
        self.category = json["pretty"].stringValue
        self.name = json["pretty"].stringValue
    }
}
