//
//  User+CoreDataProperties.swift
//  Wandr
//
//  Created by Jun Zhou on 3/13/18.
//  Copyright © 2018 Wandr. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var birthDate: NSDate?
    @NSManaged public var emailAddress: String?
    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var password: String?
    @NSManaged public var pid: String?
    @NSManaged public var profileImage: NSData?
    @NSManaged public var uid: String?

}
