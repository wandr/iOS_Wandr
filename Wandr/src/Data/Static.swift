//
//  Static.swift
//  Wandr
//
//  Created by Jun Zhou on 1/9/18.
//  Copyright © 2018 Wandr. All rights reserved.
//

import Foundation
import CoreStore

public struct Static {
    // static let userConfiguration = "Default"
    static let wandrStack: DataStack = {
        let dataStack = DataStack(xcodeModelName: "Wandr")
        try! dataStack.addStorageAndWait(
            SQLiteStore(
                fileName: "WandrStore.sqlite",
                localStorageOptions: .recreateStoreOnModelMismatch
            )
        )
        return dataStack
    }()
}
