//
//  ActivityTableViewCell.swift
//  Wandr
//
//  Created by Jun Zhou on 1/20/18.
//  Copyright © 2018 Wandr. All rights reserved.
//

import UIKit
import CHIPageControl
import Cosmos

class ActivityTableViewCell: UITableViewCell {
    
    var numberOfPages: Int = 0
    var activity: WandrActivity!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var pageControl: CHIPageControlJaloro!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var ratingControl: CosmosView!
    @IBOutlet weak var transportationLabel: UILabel!
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var transportationIcon: UIView!
    @IBOutlet weak var photoCollectionView: UICollectionView!
    
    func configureCell(activity: WandrActivity, mapElement: MapElement? = nil) {
        self.activity = activity;
        photoCollectionView.dataSource = self
        photoCollectionView.delegate = self
        nameLabel.text = activity.name
        locationLabel.text = activity.formattedAddress
        if let wandrCategory = activity.wandrCategory {
            categoryLabel.text = wandrCategory.name
        } else {
            categoryLabel.text = "Unkown"
        }
        if let hours = activity.openingHours  {
            if let close = hours.open {
                hoursLabel.text = "Close at \(close)"
            }
        } else {
            self.hoursLabel.text = "Unknown"
        }
        if let photos = self.activity.photos {
            if photos.count > 0 {
                self.numberOfPages = photos.count
            } else {
                self.numberOfPages = 1
            }
        }
        pageControl.numberOfPages = self.numberOfPages
        ratingControl.rating = Double(activity.rating)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapLikeImageView))
        likeImageView.addGestureRecognizer(tapGestureRecognizer)
        var transportationMessage = ""
        if let element = mapElement {
            if let distance = element.distanceText, let duration = element.durationText {
                transportationMessage = "\(distance) away; \(duration) drive"
                transportationIcon.isHidden = false
            }
        } else {
            transportationIcon.isHidden = true
        }
        transportationLabel.text = transportationMessage
    }
    
    @objc func tapLikeImageView() {
        if let like = self.activity.like {
            if like.boolValue {
                likeImageView.image = UIImage(named: "Heart_Icon")
                self.activity.like = false
            } else {
                likeImageView.image = UIImage(named: "Like_Icon")
                self.activity.like = true
            }
        }
    }
}

extension ActivityTableViewCell:  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.numberOfPages
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath)
        let photoImageView = cell.viewWithTag(1) as! UIImageView
        if let photos = self.activity.photos {
            if photos.count > 0 {
                let photo = photos.allObjects[indexPath.row] as! Photo
                if photo.photo == nil {
                    if let reference = photo.reference {
                        let path = "\(APIManager.firebasePhotoBaseUrl)?maxwidth=\(Int(photoImageView.frame.width))&photoreference=\(reference)&key=\(APIManager.firebaseAppKey)"
                        photoImageView.downloadedFrom(link: path, photo: photo)
                    }
                } else {
                    photoImageView.image = UIImage(data: photo.photo! as Data)
                }
            } else {
                photoImageView.image = UIImage(named: "Add_Image")
            }
        }
        return cell
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let total = scrollView.contentSize.width - scrollView.bounds.width
        let offset = scrollView.contentOffset.x
        let percent = Double(offset / total)
        let progress = percent * Double(self.numberOfPages - 1)
        self.pageControl.progress = progress
    }
}
