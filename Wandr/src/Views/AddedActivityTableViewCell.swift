//
//  AddedActivityTableViewCell.swift
//  Wandr
//
//  Created by Jun Zhou on 1/16/18.
//  Copyright © 2018 Wandr. All rights reserved.
//

import UIKit

class AddedActivityTableViewCell: UITableViewCell {
    
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var activityLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
