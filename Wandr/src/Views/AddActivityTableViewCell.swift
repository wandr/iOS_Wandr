//
//  AddActivityTableViewCell.swift
//  Wandr
//
//  Created by Jun Zhou on 1/13/18.
//  Copyright © 2018 Wandr. All rights reserved.
//

import UIKit

class AddActivityTableViewCell: UITableViewCell {
    @IBOutlet weak var addActivityButtonImage: UIImageView!
    @IBOutlet weak var activityLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
