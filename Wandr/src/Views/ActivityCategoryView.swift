//
//  ActivityCategoryView.swift
//  Wandr
//
//  Created by Jun Zhou on 1/15/18.
//  Copyright © 2018 Wandr. All rights reserved.
//

import UIKit
import CoreStore
import SwiftyJSON

protocol ActivityCategoryViewDelegate: class {
    func addWandrCategory(wandrCategory: WandrCategory)
}

class ActivityCategoryView: UIView {
    weak var delegate: ActivityCategoryViewDelegate? = nil
    var activityCategoryTableView: UITableView!
    var categories: [WandrCategory]!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        activityCategoryTableView = UITableView(frame: self.bounds, style: .plain)
        activityCategoryTableView.delegate = self
        activityCategoryTableView.dataSource = self
        _ = try? Static.wandrStack.perform(
            synchronous: { (transaction) in
                guard let wandrCategories = Static.wandrStack.fetchAll(From(WandrCategory.self)) else {
                    return
                }
                
                self.categories = wandrCategories.sorted(by: { $0.categoryId! < $1.categoryId! })
        })
        let nib = UINib(nibName: "ActivityCategoryTableViewCell", bundle: nil)
        activityCategoryTableView.register(nib, forCellReuseIdentifier: "cell")
        self.addSubview(self.activityCategoryTableView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ActivityCategoryView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.addWandrCategory(wandrCategory: self.categories[indexPath.row])
    }
}

extension ActivityCategoryView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Activities"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ActivityCategoryTableViewCell
        cell.activityLabel.text = categories[indexPath.row].name
        cell.selectionStyle = .none
        return cell
    }
    
    
}
