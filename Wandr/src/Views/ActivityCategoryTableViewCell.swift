//
//  ActivityCategoryTableViewCell.swift
//  Wandr
//
//  Created by Jun Zhou on 1/15/18.
//  Copyright © 2018 Wandr. All rights reserved.
//

import Foundation
import UIKit

class ActivityCategoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var activityLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
