//
//  EnableNotificationsViewController.swift
//  Wandr
//
//  Created by Jun Zhou on 1/9/18.
//  Copyright © 2018 Wandr. All rights reserved.
//

import UIKit
import CoreStore
import UserNotifications

class EnableNotificationsViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func openSetting(_ sender: UIButton) {
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
            // Enable or disable features based on authorization.
            if granted {
                _ = try? Static.wandrStack.perform(
                    synchronous: { (transaction) in
                        transaction.deleteAll(From<User>())
                })
                DispatchQueue.main.async {
                    self.alertCreateSucceed()
                }
            }
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    @IBAction func skip(_ sender: UIButton) {
        _ = try? Static.wandrStack.perform(
            synchronous: { (transaction) in
                transaction.deleteAll(From<User>())
        })
        DispatchQueue.main.async {
            self.alertCreateSucceed()
        }
    }
    
    func alertCreateSucceed() {
        let alert = UIAlertController(title: "Create Succeed", message: "You wandr account is ready. Please login.", preferredStyle: .alert)
        let positiveAction = UIAlertAction(title: "OK", style: .default, handler: { (alertAction) in
            super.goToLogin()
        })
        alert.addAction(positiveAction)
        present(alert, animated: true, completion: nil)
    }
}
