//
//  CreateWandrViewController.swift
//  Wandr
//
//  Created by Jun Zhou on 1/11/18.
//  Copyright © 2018 Wandr. All rights reserved.
//

import UIKit
import DropDown
import CNPPopupController
import CoreStore
import SwiftyJSON
import FirebaseAuth
import FirebaseDatabase
import SwiftLocation
import CoreLocation

class CreateWandrViewController: WandrBaseViewController, ActivityCategoryViewDelegate {
    
    @IBOutlet weak var wandrButton: UIButton!
    @IBOutlet weak var locationTextField: DTTextField!
    @IBOutlet weak var wandrActivityTableView: UITableView!
    var popupController: CNPPopupController?
    var ref: DatabaseReference!
    
    var isSynced = false
    var timer: Timer?
    var addedWandrCategories: [WandrCategory]!
    var citiesName: [String]!
    var cities: [UsaCity]!
    var selectedCity: UsaCity!
    let activityLocationDropDown = DropDown()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var currentLocation: CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let dismissKeyboardTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(endEditing))
        dismissKeyboardTapGestureRecognizer.cancelsTouchesInView  = false
        self.view.addGestureRecognizer(dismissKeyboardTapGestureRecognizer)
        self.ref = Database.database().reference()
        getCategories()
        configureActivityDropDown()
        locationTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        initData()
        Locator.currentPosition(accuracy: .block, onSuccess: {
            (locaiton) in
            print("Location found: \(locaiton)")
            self.currentLocation = locaiton
        }, onFail: {
            (err, last) in
            print("Failed to getlocation: \(err)")
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.wandrActivityTableView.allowsMultipleSelection = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getCategories() {
        self.ref.child("places").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let children = snapshot.children.allObjects as! [DataSnapshot]
            for child in children {
                let key = child.key
                let json = JSON(child.value as Any)
                _ = try? Static.wandrStack.perform(
                    synchronous: { (transaction) in
                        let wandrCategory = transaction.create(Into<WandrCategory>())
                        wandrCategory.fromJSON(json, keyValue: key)
                })
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func initData() {
        addedWandrCategories = [WandrCategory]()
        cities = []
        citiesName = []
        _ = try? Static.wandrStack.perform(
            synchronous: { (transaction) in
                transaction.deleteAll(From<WandrCategory>())
                transaction.deleteAll(From<WandrActivity>())
        })
    }
    
    func configureActivityDropDown() {
        activityLocationDropDown.anchorView = locationTextField
        activityLocationDropDown.direction = DropDown.Direction.bottom
        activityLocationDropDown.bottomOffset = CGPoint(x: 0, y:(activityLocationDropDown.anchorView?.plainView.bounds.height)!)
        // TODO: Call api to get location list
        activityLocationDropDown.dataSource = []
        activityLocationDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.locationTextField.resignFirstResponder()
            self.locationTextField.text = item
            self.selectedCity = self.cities[index]
        }
    }

    override func popSelf() {
        let alert = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: .alert)
        let positiveAction = UIAlertAction(title: "Yes", style: .default, handler: { (alert: UIAlertAction) in
            self.deleteAllSelectedCategoris()
            self.getCategories()
            self.isHiddenWandrButton()
            self.addedWandrCategories.removeAll()
            self.wandrActivityTableView.reloadData()
            self.logout()
        })
        let negativeAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(positiveAction)
        alert.addAction(negativeAction)
        present(alert, animated: true, completion: nil)
    }
    
    @objc func findCity(_ timer: Timer) {
        let searchQuery = timer.userInfo! as! String
        cities.removeAll()
        citiesName.removeAll()
        Locator.api.googleAPIKey = APIManager.googleAPIKey
        if let location = self.currentLocation {
            let coordinates = location.coordinate
            Locator.location(fromCoordinates: coordinates, using: .google, onSuccess: { places in
                if let place = places.first, let formattedAddress = place.formattedAddress {
                    self.citiesName.append(formattedAddress)
                }
                DispatchQueue.main.async() {
                    self.activityLocationDropDown.dataSource = self.citiesName
                    self.activityLocationDropDown.show()
                }
            }) { err in
                print(err)
            }
        }
        Locator.location(fromAddress: searchQuery, onSuccess: {
            candidates in
            print("Found \(candidates.count) candidates for this search")
            // get the detail of the first candidate - return a Place object.
            for city in candidates {
                if let detail = city.city {
                    self.citiesName.append(detail)
                }
            }
            self.activityLocationDropDown.dataSource = self.citiesName
            self.activityLocationDropDown.show()
        }, onFail: {err in
            print(err)
        })
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let searchQuery = textField.text else {
            return
        }
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(findCity(_:)), userInfo: searchQuery, repeats: false)
    }
    
    @objc func endEditing() {
       locationTextField.resignFirstResponder()
    }
    
    func logout() {
        // TODO: Add logout function
        print("Logout user")
        _ = try? Static.wandrStack.perform(
            synchronous: { (transaction) in
                transaction.deleteAll(From<User>())
        })
        self.dismiss(animated: true, completion: nil)
    }
    
    func deleteAllSelectedCategoris() {
        _ = try? Static.wandrStack.perform(
            synchronous: { (transaction) in
                transaction.deleteAll(From<WandrCategory>())
                transaction.deleteAll(From<WandrActivity>())
        })
    }
    
    @IBAction func tapWandrButton(_ sender: Any) {
        // TODO:Go to Wandr output
        // Call create wandr api
        if isValid() {
            getWandrActivies(wandrCategories: self.addedWandrCategories)
        }
    }
    
    func isValidValue(json: JSON) -> Bool {
        if json["map"].isEmpty {
            return false
        }
        return true
    }
    
    func isValid() -> Bool {
        if let location = locationTextField.text {
            if !location.isEmpty {
                return true
            }
        }
        alertErrorMessage(message: "Location is empty.")
        return false
    }
    
    func isHiddenWandrButton() {
        if self.addedWandrCategories.count > 0 {
            wandrButton.isHidden = false
        } else {
            wandrButton.isHidden = true
        }
    }
    
    func  getLocation() -> [Double] {
        return [self.selectedCity.latitude, self.selectedCity.longitude]
    }
    
    func getRadius() -> Int {
        return 20000
    }
    
    func getWandrActivies(wandrCategories: [WandrCategory]) {
        guard let user = appDelegate.currentUser else {
            return
        }
        var temp: [String] = []
        for category in wandrCategories {
            if let name = category.categoryId {
                temp.append(name)
            }
        }
        let ref = self.ref.child("wanders").childByAutoId()
        let json: NSDictionary = [
            "placeTypes": temp,
            "author": user.pid!,
            "location": getLocation(),
            "radius": getRadius(),
            "modified": [".sv": "timestamp"],
            "created": [".sv": "timestamp"]
        ]
        ref.setValue(json)
        ref.observe(.value, with: { (snapshot) in
            guard let activity = snapshot.value as? NSDictionary else {
                super.alertErrorMessage(message: NSLocalizedString("NoActivities", comment: "no activities for this users"))
                return
            }
            let json = JSON(activity)
            print(json)
            if !self.isValidValue(json: json) {
                return
            }
            _ = Static.wandrStack.perform(
                asynchronous: { (transaction) in
                    transaction.deleteAll(From<Wander>())
                    let wander = transaction.create(Into<Wander>())
                    wander.fromJSONWithTransaction(json, keyValue: snapshot.key, transaction: transaction)
            }, completion: { _ in
                self.performSegue(withIdentifier: "goToOutputWandrViewController", sender: nil)
            })
            print("get user info successful!")
        })
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return self.isSynced
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "goToOutputWandrViewController" {
            if segue.destination is OutputWandrViewController {
                //TODO: Add user info
                let outputWandrViewController = segue.destination as! OutputWandrViewController
                outputWandrViewController.selectedCity = self.selectedCity
            }
        }
    }
}

extension CreateWandrViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row >= self.addedWandrCategories.count {
            self.showPopupWithStyle(CNPPopupStyle.actionSheet)
        }
    }
}

extension CreateWandrViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.addedWandrCategories.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < self.addedWandrCategories.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "addedActivityCell", for: indexPath) as! AddedActivityTableViewCell
            cell.selectionStyle = .none
            cell.activityLabel.text = self.addedWandrCategories[indexPath.row].category
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "addActivityCell", for: indexPath)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row == self.addedWandrCategories.count {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.addedWandrCategories.remove(at: indexPath.row)
            isHiddenWandrButton()
            tableView.reloadData()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

extension CreateWandrViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension CreateWandrViewController : CNPPopupControllerDelegate {
    func showPopupWithStyle(_ popupStyle: CNPPopupStyle) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping
        paragraphStyle.alignment = NSTextAlignment.center
        
        let customView = ActivityCategoryView.init(frame: CGRect(x: 0, y: 0, width: 375, height: 440))
        customView.delegate = self
        customView.backgroundColor = UIColor.white

        let popupController = CNPPopupController(contents:[customView])
        popupController.theme = CNPPopupTheme.default()
        popupController.theme.popupStyle = popupStyle
        // LFL added settings for custom color and blur
        popupController.theme.maskType = .dimmed
        popupController.delegate = self
        self.popupController = popupController
        popupController.present(animated: true)
    }
    
    func popupControllerWillDismiss(_ controller: CNPPopupController) {
        print("Popup controller will be dismissed")
    }
    
    func popupControllerDidPresent(_ controller: CNPPopupController) {
        print("Popup controller presented")
    }
    
    func addWandrCategory(wandrCategory: WandrCategory) {
        self.addedWandrCategories.append(wandrCategory)
        self.wandrActivityTableView.reloadData()
        isHiddenWandrButton()
        self.popupController?.dismiss(animated: true)
    }
    
}
