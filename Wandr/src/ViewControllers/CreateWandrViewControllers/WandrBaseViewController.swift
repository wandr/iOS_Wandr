//
//  WandrBaseViewController.swift
//  Wandr
//
//  Created by Jun Zhou on 1/13/18.
//  Copyright © 2018 Wandr. All rights reserved.
//

import UIKit

class WandrBaseViewController: UIViewController {

    var currentUser: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.black
        let backBtn = UIBarButtonItem(image: UIImage(named: "Back_Arrow")!,
                                      style: .plain, target: self, action: #selector(popSelf))
        self.navigationItem.leftBarButtonItem = backBtn
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func alertErrorMessage(message: String) {
        let alert = UIAlertController(title: NSLocalizedString("Error", comment: "error title"), message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func popSelf() {
        self.navigationController?.popViewController(animated: true)
    }
}
