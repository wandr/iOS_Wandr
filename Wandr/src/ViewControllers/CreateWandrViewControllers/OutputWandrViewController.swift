//
//  WandrOutPutViewController.swift
//  Wandr
//
//  Created by Jun Zhou on 1/16/18.
//  Copyright © 2018 Wandr. All rights reserved.
//

import UIKit
import CoreStore

class OutputWandrViewController: WandrBaseViewController {
    
    @IBOutlet weak var selectedLocationTextField: DTTextField!
    @IBOutlet weak var activityTableView: UITableView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var wandrButton: UIButton!
    var wander: Wander!
    var selectedCity: UsaCity!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let n = selectedCity.name, let s = selectedCity.state {
            self.selectedLocationTextField.text  = n + ", " + s
        }
        initData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initData() {
        let from = From<Wander>()
        if let wander = Static.wandrStack.fetchAll(from) {
            self.wander = wander[0]
            self.activityTableView.reloadData()
        }
    }
}

extension OutputWandrViewController: UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
}

extension OutputWandrViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let activities = self.wander.wandrActivities {
            return activities.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ActivityTableViewCell
        if let activities = self.wander.wandrActivities, let map = self.wander.map {
            let activity = activities.allObjects[indexPath.row] as! WandrActivity
            if indexPath.row < activities.count - 1 {
                if let mapElements = map.mapElements {
                    let mapElement = mapElements.allObjects[indexPath.row] as! MapElement
                    cell.configureCell(activity: activity, mapElement: mapElement)
                }
            } else {
                cell.configureCell(activity: activity, mapElement: nil)
            }
        }
        cell.selectionStyle = .none
        return cell
    }
}
