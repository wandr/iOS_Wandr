//
//  LoadingViewController.swift
//  Wandr
//
//  Created by Jun Zhou on 4/18/18.
//  Copyright © 2018 Wandr. All rights reserved.
//

import Foundation
import UIKit
import CoreStore

class LoadingViewController: UIViewController {
    
    @IBOutlet weak var loadingProgressView: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadUSACitiesSync()
    }
    
    func loadUSACities() {
        _ = try? Static.wandrStack.perform(
            synchronous: { (transaction)  in
                transaction.deleteAll(From<UsaCity>())
                let pathURL = URL(fileURLWithPath: Bundle.main.path(forResource: "usacities", ofType: "txt")!)
                let s = StreamReader(url: pathURL)
                for _ in 1...141989 {
                    if let line = s?.nextLine() {
                        let temp = line.components(separatedBy: "\t")
                        let city = transaction.create(Into<UsaCity>())
                        city.name = temp[0]
                        city.state = temp[1]
                        if let latitude = Double(temp[2]) {
                            city.latitude = latitude
                        }
                        if let longitude = Double(temp[3]) {
                            city.longitude = longitude
                        }
                    }
                }
        })
    }
    
    func loadUSACitiesSync() {
        let pathURL = URL(fileURLWithPath: Bundle.main.path(forResource: "usacities", ofType: "txt")!)
        let s = StreamReader(url: pathURL)
        for i in 1...141989 {
            DispatchQueue.main.async() {
                if i % 3 == 0 {
                    self.loadingProgressView.setProgress(Float(i) / Float(141989), animated: true)
                }
            }
            _ = try? Static.wandrStack.perform(
                synchronous: { (transaction)  in
                    transaction.deleteAll(From<UsaCity>())
                        if let line = s?.nextLine() {
                            let temp = line.components(separatedBy: "\t")
                            let city = transaction.create(Into<UsaCity>())
                            city.name = temp[0]
                            city.state = temp[1]
                            if let latitude = Double(temp[2]) {
                                city.latitude = latitude
                            }
                            if let longitude = Double(temp[3]) {
                                city.longitude = longitude
                            }
                        }
            })
        }
    }
}
