//
//  BirthDateViewController.swift
//  Wandr
//
//  Created by Jun Zhou on 1/11/18.
//  Copyright © 2018 Wandr. All rights reserved.
//

import UIKit
import CoreStore

class BirthDateViewController: CreateAccountBaseViewController {
    
    @IBOutlet weak var birthDateTextField: UITextField!
    @IBOutlet weak var birthDatePicker: UIDatePicker!
    
    var birthDate: NSDate?
    var isValid = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nextButton.layer.cornerRadius = 5;
        self.nextButton.backgroundColor = UIColor.FlatColor.Blue.BlockNextBlue
        self.birthDatePicker.addTarget(self,
                                       action: #selector(datePickerChanged(_:)),
                                       for: .valueChanged)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func datePickerChanged(_ datePicker: UIDatePicker) {
        self.isValid = true
        super.changeNextButton(!self.isValid)
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        self.birthDateTextField.text = dateFormatter.string(from: datePicker.date)
        self.birthDate = NSDate(timeIntervalSince1970: datePicker.date.timeIntervalSince1970)
    }
    
    override func shouldPerformSegue(withIdentifier
        identifier: String, sender: Any?) -> Bool {
        return self.isValid
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToFinishTouchesVC" {
            if segue.destination is FinishTouchesViewController {
                // TODO: Add user info
                _ = try? Static.wandrStack.perform(
                    synchronous: { (transaction) in
                        guard let user = Static.wandrStack.fetchOne(From(User.self)) else {
                            return
                        }
                        user.birthDate = self.birthDate
                })
            }
        }
    }
}
