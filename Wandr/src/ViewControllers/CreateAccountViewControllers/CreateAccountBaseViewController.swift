//
//  CreateAccountBaseViewController.swift
//  Wandr
//
//  Created by Jun Zhou on 1/11/18.
//  Copyright © 2018 Wandr. All rights reserved.
//

import UIKit

class CreateAccountBaseViewController: BaseViewController {
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var nextButtonToBottomConstraint: NSLayoutConstraint!
    
    var user: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nextButton.layer.cornerRadius = 5;
        self.nextButton.backgroundColor = UIColor.FlatColor.Blue.BlockNextBlue
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardDidShow(_:)),
                                               name: .UIKeyboardDidShow , object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardDidHide(_:)),
                                               name: .UIKeyboardDidHide , object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.UIKeyboardDidShow,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.UIKeyboardDidHide,
                                                  object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func keyboardDidShow(_ notification: NSNotification) {
        print("Keyboard will show!")
        let keyboardSize:CGSize = (notification.userInfo![UIKeyboardFrameEndUserInfoKey]
            as! NSValue).cgRectValue.size
        print("Keyboard size: \(keyboardSize)")
        let height = min(keyboardSize.height, keyboardSize.width)
        guard self.nextButtonToBottomConstraint != nil else {
            return
        }
        self.nextButtonToBottomConstraint.constant = height + 20
    }
    
    @objc func keyboardDidHide(_ notification: NSNotification) {
        print("Keyboard will hide!")
        guard self.nextButtonToBottomConstraint != nil else {
            return
        }
        self.nextButtonToBottomConstraint.constant = 20
    }
    
    func changeNextButton(_ isBlocked: Bool) {
        if isBlocked {
            self.nextButton.backgroundColor = UIColor.FlatColor.Blue.BlockNextBlue
        } else {
            self.nextButton.backgroundColor = UIColor.FlatColor.Blue.MainAppBlue
        }
    }
}
