//
//  CreateAccountViewController.swift
//  Wandr
//
//  Created by Jun Zhou on 1/9/18.
//  Copyright © 2018 Wandr. All rights reserved.
//

import UIKit
import CoreStore

class CreateAccountViewController: UIViewController {
    
    @IBOutlet weak var createAccountButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createAccountButton.layer.cornerRadius = 5
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        guard let user = Static.wandrStack.fetchAll(From(User.self)) else {
            return
        }
        if !user.isEmpty {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.currentUser = user.first
            let homeStoryBoard = UIStoryboard(name: "Home", bundle: nil)
            if let homeNavigationViewController
                = homeStoryBoard.instantiateViewController(withIdentifier: "HomeNavigationViewController") as? UINavigationController {
                self.present(homeNavigationViewController, animated: true, completion: nil)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func createAccount(_ sender: UIButton) {
        print("create a new account, go to name vc")
        // make create account request?
    }
    
    @IBAction func login(_ sender: UIButton) {
        print("login directly")
    }
}
