//
//  EmailViewController.swift
//  Wandr
//
//  Created by Jun Zhou on 1/11/18.
//  Copyright © 2018 Wandr. All rights reserved.
//

import UIKit
import CoreStore

class EmailViewController: CreateAccountBaseViewController {
    
    @IBOutlet weak var emailAddressTextField: DTTextField!
    var emailAddress: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.emailAddressTextField.autocorrectionType = .no
        self.emailAddressTextField.addTarget(self,
                                             action: #selector(textFieldDidChange(_:)),
                                             for: .editingChanged)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.emailAddressTextField.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        super.changeNextButton(checkTextFieldIsBlocked())
    }
    
    func checkTextFieldIsBlocked() -> Bool {
        guard let emailAddress = self.emailAddressTextField.text else {
            return true
        }
        if !emailAddress.isEmptyStr {
            if emailAddress.isValidEmail() {
                self.emailAddress = emailAddress
            }
            return !emailAddress.isValidEmail()
        }
        return true
    }
    
    override func shouldPerformSegue(withIdentifier
        identifier: String, sender: Any?) -> Bool {
        return !checkTextFieldIsBlocked()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToCreatePasswordVC" {
            if segue.destination is CreatePasswordViewController {
                // TODO: Add user info
                _ = try? Static.wandrStack.perform(
                    synchronous: { (transaction) in
                        guard let user = Static.wandrStack.fetchOne(From(User.self)) else {
                            return
                        }
                        user.emailAddress = self.emailAddress
                })
            }
        }
    }
}
