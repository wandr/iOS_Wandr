//
//  UserNameViewController.swift
//  Wandr
//
//  Created by Jun Zhou on 1/11/18.
//  Copyright © 2018 Wandr. All rights reserved.
//

import UIKit
import CoreStore

class UserNameViewController: CreateAccountBaseViewController {
    
    @IBOutlet weak var firstNameTextField: DTTextField!
    @IBOutlet weak var lastNameTextField: DTTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.firstNameTextField.autocorrectionType = .no
        self.lastNameTextField.autocorrectionType = .no
        self.firstNameTextField.addTarget(self,
                                          action: #selector(textFieldDidChange(_:)),
                                          for: .editingChanged)
        self.lastNameTextField.addTarget(self,
                                         action: #selector(textFieldDidChange(_:)),
                                         for: .editingChanged)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.firstNameTextField.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        super.changeNextButton(checkTextFieldIsBlocked())
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return !checkTextFieldIsBlocked()
    }
    
    func checkTextFieldIsBlocked() -> Bool {
        guard let firstName = self.firstNameTextField.text, let lastName = self.lastNameTextField.text else {
            return true
        }
        if (!firstName.isEmptyStr && !lastName.isEmptyStr) {
            _ = try? Static.wandrStack.perform(
                synchronous: { (transaction) in
                    transaction.deleteAll(From<User>())
                    let user = transaction.create(Into<User>())
                    user.firstName = firstName.trimmingCharacters(in: .whitespacesAndNewlines)
                    user.lastName = lastName.trimmingCharacters(in: .whitespacesAndNewlines)
            })
            return false
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToEmailVC" {
            if segue.destination is EmailViewController {
                //TODO: Add user info
                
            }
        }
    }
    
    override func popSelf() {
        _ = try? Static.wandrStack.perform(
            synchronous: { (transaction) in
                transaction.deleteAll(From<User>())
        })
        super.popSelf()
    }
}

