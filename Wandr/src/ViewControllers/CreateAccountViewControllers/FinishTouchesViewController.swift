//
//  FinishTouchViewController.swift
//  Wandr
//
//  Created by Jun Zhou on 1/11/18.
//  Copyright © 2018 Wandr. All rights reserved.
//


import UIKit
import CoreStore
import Foundation
import FirebaseDatabase
import FirebaseAuth

class FinishTouchesViewController: BaseViewController {
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameTextField: DTTextField!
    @IBOutlet weak var ageTextField: DTTextField!
    @IBOutlet weak var emailTextField: DTTextField!
    
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapOnProfileImageView(_:)))
        self.profileImageView.addGestureRecognizer(tapGestureRecognizer)
        self.initView()
        self.ref = Database.database().reference()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.profileImageView.layer.borderWidth = 1
        self.profileImageView.layer.masksToBounds = false
        self.profileImageView.layer.borderColor = UIColor.clear.cgColor
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.height / 2
        self.profileImageView.clipsToBounds = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        _ = try? Static.wandrStack.perform(
            synchronous: { (transaction) in
                guard let user = Static.wandrStack.fetchOne(From(User.self)) else {
                    return
                }
                self.nameTextField.text = user.firstName! + " " + user.lastName!
                self.emailTextField.text = user.emailAddress
                guard let birthDate = user.birthDate else {
                    return
                }
                let calendar = Calendar.current
                let ageComponents = calendar.dateComponents([.year], from: birthDate as Date, to: Date())
                let age = ageComponents.year!
                self.ageTextField.text = "\(age)"
        })
    }
    
    @objc func tapOnProfileImageView(_ imageView: UIImageView) {
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let openCameraAction = UIAlertAction(title: NSLocalizedString("Camera", comment: "camera button"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openCamera()
        })
        let openPhotoLibraryAction = UIAlertAction(title: NSLocalizedString("PhotoLibrary", comment: "photo library button"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openPhotoLibrary()
        })
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "cancel button"), style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            optionMenu.dismiss(animated: true, completion: nil)
        })
        optionMenu.addAction(openCameraAction)
        optionMenu.addAction(openPhotoLibraryAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    override func shouldPerformSegue(withIdentifier
        identifier: String, sender: Any?) -> Bool {
        return !UIApplication.shared.isRegisteredForRemoteNotifications
    }
    
    @IBAction func createAccount(_ sender: UIButton) {
        self.view.addSubview(self.progressIndicator)
        _ = try? Static.wandrStack.perform(
            synchronous: { (transaction) in
                guard let user = Static.wandrStack.fetchOne(From(User.self)) else {
                    self.progressIndicator.removeFromSuperview()
                    return
                }
                Auth.auth().createUser(withEmail: user.emailAddress!, password: user.password!) { (u, error) in
                    guard let e = error else {
                        guard let temp = u else {
                            self.progressIndicator.removeFromSuperview()
                            return
                        }
                        user.uid = temp.user.uid
                        self.login(currentUser: user, completion: ({(result) -> Void in
                            self.progressIndicator.removeFromSuperview()
                            guard let r = result else {
                                self.checkEnableNotification()
                                return
                            }
                            super.alertErrorMessage(message: r)
                            return
                        }))
                        return
                    }
                    super.alertErrorMessage(message: e.localizedDescription)
                    return
                }
        })
    }
    
    func login(currentUser: User, completion: @escaping (_ result: String?) -> Void) {
        Auth.auth().signIn(withEmail: currentUser.emailAddress!, password: currentUser.password!) { (user, error) in
            guard let e = error else {
                guard let temp = user else {
                    completion("No user found")
                    self.progressIndicator.removeFromSuperview()
                    return
                }
                let userID = temp.user.uid
                var body = ["firstName": currentUser.firstName!,
                             "lastName": currentUser.lastName!,
                             "emailAddress": currentUser.emailAddress!,
                             "birthDate": currentUser.birthDate?.timeIntervalSince1970 as Any,
                             "created": ServerValue.timestamp()] as [String : Any]
                if currentUser.profileImage != nil {
                    let imageString = self.compressProfileImage(imageData: currentUser.profileImage!)
                    if imageString != nil {
                        body["profileImage"] = imageString
                    }
                }
                self.ref.child("_user").child("\(userID)").setValue(body as AnyObject)
                let childId = self.ref.child("users").childByAutoId()
                let pidBody = ["pid": childId.key,
                               "modified": ServerValue.timestamp()] as [AnyHashable : Any]
                self.ref.child("_user").child("\(userID)").updateChildValues(pidBody)
                childId.setValue(body as AnyObject)
                completion(nil)
                return
            }
            completion(e.localizedDescription)
        }
    }
    
    func compressProfileImage(imageData: NSData) -> String? {
        let image = UIImage(data: imageData as Data)
        return image?.jpeg(.lowest)?.base64EncodedString()
    }
    
    func checkEnableNotification() {
        let isRegisteredForRemoteNotifications = UIApplication.shared.isRegisteredForRemoteNotifications
        if isRegisteredForRemoteNotifications {
            // User is registered for notification
            let homeStoryBoard = UIStoryboard(name: "Home", bundle: nil)
            if let tabViewController
                = homeStoryBoard.instantiateViewController(withIdentifier: "homeTabBarController") as? UITabBarController {
                self.present(tabViewController, animated: true, completion: nil)
            }
        } else {
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            if let enableNotificationViewController
                = mainStoryBoard.instantiateViewController(withIdentifier: "enableNotificationVC") as? UIViewController {
                self.present(enableNotificationViewController, animated: true, completion: nil)
            }
        }
    }
}

extension FinishTouchesViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.profileImageView.image = image
        _ = try? Static.wandrStack.perform(
            synchronous: { (transaction) in
                guard let user = Static.wandrStack.fetchOne(From(User.self)) else {
                    return
                }
                guard let data = UIImagePNGRepresentation(image) as NSData? else {
                    return
                }
                user.profileImage = data;
        })
        dismiss(animated:true, completion: nil)
    }
}
