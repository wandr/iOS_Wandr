//
//  CreatePasswordViewController.swift
//  Wandr
//
//  Created by Jun Zhou on 1/11/18.
//  Copyright © 2018 Wandr. All rights reserved.
//

import UIKit
import CoreStore

class CreatePasswordViewController: CreateAccountBaseViewController {
    @IBOutlet weak var passwordTextField: DTTextField!
    @IBOutlet weak var specialCharacterErrorLabel: UILabel!
    @IBOutlet weak var uppercaseErrorLabel: UILabel!
    @IBOutlet weak var lowercaseErrorLabel: UILabel!
    @IBOutlet weak var characterNumberErrorLabel: UILabel!
    
    var showPassword = false
    var isValid = false
    var password: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.passwordTextField.autocorrectionType = .no
        self.passwordTextField.addTarget(self,
                                         action: #selector(textFieldDidChange(_:)),
                                         for: .editingChanged)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.passwordTextField.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isCharacterNumberValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format:
            "SELF MATCHES %@", "^.{8,}$")
        return passwordTest.evaluate(with: password)
    }
    
    func isUppercaseValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format:
            "SELF MATCHES %@", ".*[A-Z]+.*")
        return passwordTest.evaluate(with: password)
    }
    
    func isLowercaseValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format:
            "SELF MATCHES %@", "^.*(?=.*[a-z]).*$")
        return passwordTest.evaluate(with: password)
    }
    
    func isSpecialCharacterValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format:
            "SELF MATCHES %@", "^.*(?=.*[!@#$&*]).*$")
        return passwordTest.evaluate(with: password)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let password = self.passwordTextField.text else {
            return
        }
        if self.characterNumberErrorLabel.isHidden {
            self.characterNumberErrorLabel.isHidden = false
            self.uppercaseErrorLabel.isHidden = false
            self.lowercaseErrorLabel.isHidden = false
            self.specialCharacterErrorLabel.isHidden = false
        }
        changeErrorStatus(password: password)
    }
    
    func changeErrorStatus(password: String) {
        if self.isCharacterNumberValid(password) {
            self.isValid = true
            self.characterNumberErrorLabel.textColor = UIColor.FlatColor.Green.PasswordCorrectGreen
        } else {
            self.isValid = false
            self.characterNumberErrorLabel.textColor = UIColor.FlatColor.Red.PasswordErrorRed
        }
        if self.isUppercaseValid(password) {
            self.isValid = true
            self.uppercaseErrorLabel.textColor = UIColor.FlatColor.Green.PasswordCorrectGreen
        } else {
            self.isValid = false
            self.uppercaseErrorLabel.textColor = UIColor.FlatColor.Red.PasswordErrorRed
        }
        if self.isLowercaseValid(password) {
            self.isValid = true
            self.lowercaseErrorLabel.textColor = UIColor.FlatColor.Green.PasswordCorrectGreen
        } else {
            self.isValid = false
            self.lowercaseErrorLabel.textColor = UIColor.FlatColor.Red.PasswordErrorRed
        }
        if self.isSpecialCharacterValid(password) {
            self.isValid = true
            self.specialCharacterErrorLabel.textColor = UIColor.FlatColor.Green.PasswordCorrectGreen
        } else {
            self.isValid = false
            self.specialCharacterErrorLabel.textColor = UIColor.FlatColor.Red.PasswordErrorRed
        }
        if (self.isValid) {
            self.password = password
        }
        super.changeNextButton(!self.isValid)
    }
    
    @IBAction func showPassword(_ sender: UIButton) {
        if self.showPassword {
            self.showPassword = false
            self.passwordTextField.isSecureTextEntry = true
        } else {
            self.showPassword = true
            self.passwordTextField.isSecureTextEntry = false
        }
    }
    
    override func shouldPerformSegue(withIdentifier
        identifier: String, sender: Any?) -> Bool {
        return self.isValid
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToBirthdateVC" {
            if segue.destination is BirthDateViewController {
                // TODO: Add user info
                _ = try? Static.wandrStack.perform(
                    synchronous: { (transaction) in
                        guard let user = Static.wandrStack.fetchOne(From(User.self)) else {
                            return
                        }
                        user.password = self.password
                })
            }
        }
    }
}
