//
//  ViewController.swift
//  Wandr
//
//  Created by Jun Zhou on 1/9/18.
//  Copyright © 2018 Wandr. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import CoreStore
import SwiftyJSON

class LoginViewController: BaseViewController {
    
    var ref: DatabaseReference!

    @IBOutlet weak var emailTextField: DTTextField!
    @IBOutlet weak var passwordTextField: DTTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.passwordTextField.isSecureTextEntry = true
        self.ref = Database.database().reference()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func login(_ sender: UIButton) {
        self.view.addSubview(self.progressIndicator)
        guard let emailAddress = emailTextField.text, let password = passwordTextField.text else {
            super.alertErrorMessage(message: NSLocalizedString("TextError", comment: "text error"))
            return
        }
        if emailAddress.isEmptyStr || password.isEmptyStr {
            super.alertErrorMessage(message: NSLocalizedString("UsernameOrPasswordIsEmpty", comment: "invalid username or password"))
            return
        }
        Auth.auth().signIn(withEmail: emailAddress, password: password) { (user, error) in
            guard let e = error else {
                guard let temp = user else {
                    super.alertErrorMessage(message: NSLocalizedString("NoUserDataOnServer", comment: "no user data when login"))
                    return
                }
                print("login successful!")
                self.ref.child("_user").child(temp.user.uid).observeSingleEvent(of: .value, with: { (snapshot) in
                    // Get user value
                    guard let privateUser = snapshot.value as? NSDictionary else {
                        super.alertErrorMessage(message: NSLocalizedString("NoUserDataOnServer", comment: "no user data when login"))
                        return
                    }
                    print("get user info successful!")
                    print(privateUser)
                    guard let pid = privateUser.value(forKey: "pid") as? String else {
                        return
                    }
                    self.ref.child("users").child(pid).observeSingleEvent(of: .value, with: { (snapshot) in
                        guard let publicUser = snapshot.value as? NSDictionary else {
                            super.alertErrorMessage(message: NSLocalizedString("NoUserDataOnServer", comment: "no user data when login"))
                            return
                        }
                        let json = JSON(publicUser as Any)
                        if !self.logInUser(json: json, pid: pid) {
                            super.alertErrorMessage(message: NSLocalizedString("NoUserDataOnServer", comment: "no user data when login"))
                        }
                        self.progressIndicator.removeFromSuperview()
                        super.goToHome()
                    }) { (error) in
                        print(error.localizedDescription)
                    }
                  
                }) { (error) in
                    self.progressIndicator.removeFromSuperview()
                    print(error.localizedDescription)
                }
                return
            }
            super.alertErrorMessage(message: e.localizedDescription)
        }
    }
    
    func logInUser(json: JSON, pid: String) -> Bool {
        _ = try? Static.wandrStack.perform(
            synchronous: { (transaction) in
                transaction.deleteAll(From<User>())
                let user = transaction.create(Into<User>())
                user.fromJSON(json, keyValue: pid)
        })
        guard let user = Static.wandrStack.fetchAll(From(User.self)) else {
            return false
        }
        if !user.isEmpty {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.currentUser = user.first
        }
        return true
    }
}

